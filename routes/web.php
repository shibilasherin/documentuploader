<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::redirect('/', 'home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('application', 'ApplicationController');
Route::post('application/getstate', 'ApplicationController@getstate')->name('application.getstate');
Route::post('application/status', 'ApplicationController@status')->name('application.status');
Route::get('register/pass', 'Auth\RegisterController@resetpass')->name('register.pass');
Route::put('register/reset', 'Auth\RegisterController@reset')->name('register.reset');
Route::get('profilesetting', 'ApplicationController@profilesetting')->name('application.profilesetting');
Route::put('updateprofile', 'ApplicationController@updateprofile')->name('application.updateprofile');

