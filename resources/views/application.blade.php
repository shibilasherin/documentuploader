
 @extends('layouts.master')

@section('content')
<div class="content-wrapper">

  <div class="content-heading">
   <span><i class="fa fa-file"></i> Submit your application</span>
 </div>

 <div class="panel panel-default">
   <div class="panel-body">
    <form class="form-horizontal" action="{{ route('application.store') }}" method="post" id="form" enctype="multipart/form-data">
    @csrf
     <div class="form-group">
       <div class="col-sm-6">
         <label class="control-label">
          First name
        </label>
        <input type="text" name="first_name" class="form-control" placeholder="First name">
      </div>
      <div class="col-sm-6">
       <label class="control-label">
         Last name
       </label>
       <input type="text" name="last_name" class="form-control" placeholder="Last name">
     </div>
   </div>
     <div class="form-group">
     <div class="col-sm-6">
       <label class="control-label">
        Father Name
      </label>
      <input type="text" name="father_name" class="form-control" placeholder="Father Name">
    </div>
  
  
    <div class="col-sm-6">
       <label class="control-label">
        Phone number
      </label>
      <input type="number" name="phone" class="form-control" placeholder="Phone number">
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-6">
     <label class="control-label">
      Email
    </label>
    <input type="text" name="email" class="form-control" placeholder="Email">
  </div>
  <div class="col-sm-6">
   <label class="control-label">
    Address
  </label>
  <input type="text" name="address" class="form-control" placeholder="Address HouseNo landmark">
</div>

</div>

<div class="form-group">
 <div class="col-sm-6">
   <label class="control-label">
     Select city
   </label>
   
     <select class="form-control select2-4" name="city_id" id="city_id">
       <option value="">Select</option>
       @foreach($city as $cit)
         <option value="{{$cit->id}}">{{$cit->name}}</option>
         @endforeach
      </select>
 </div>
 <div class="col-sm-6">
   <label class="control-label">
    State
  </label>
   <input type="text"  class="form-control" placeholder="state" name="state_id" id="state_id">
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
 <label class="control-label">
  Country
</label>
   <input type="text"  class="form-control" placeholder="Country" name="country_id" id="country_id">
  

</div>

  <div class="col-sm-6">
   <label class="control-label"> Upload Documents </label>
   {{-- <input type="file" name="documents[]" class="form-control filestyle"  onChange="readURL(this);"> --}}
   <input type="file" name="documents[]" class="form-control filestyle" onchange="preview_images();" multiple/>
 </div>
</div>
  
<div class="form-group">
 <div class="col-sm-12">
  <div class="pull-right">
   <button class="btn btn-info btn-lg" type="submit">Submit</button>
 </div>
</div>
</div>
</form>

</div>
</div>
</div>
@endsection

@section('custom_css')
  <style>
    .form-group {
      padding: 0 15px;
    }


  </style>
@endsection

@section('custom_js')
  <script src="{{ asset('js/input-values.jquery.js') }}"></script>
  <script src="{{ asset('js/bootstrap-filestyle.js') }}"></script>
 
 <script type="text/javascript">
   $('#city_id').change(function() {
     var city =$(this).val();
     // alert(city);
        var formData = new FormData();
        formData.append('city',city);
        formData.append("_token", '{{ csrf_token() }}');
     $.ajax({
       url:'{{ route('application.getstate') }}',
       type: 'POST',
       dataType: 'json',
       data:formData,
       processData: false,
       contentType: false
     })
     
     .done(function(data) {
       if(data.status=="success")
       {
      

         $("#state_id").val(data.state.name);
         $("#country_id").val(data.country.name);
             
       }
       else
       {
         $("#state_id").val();
         $("#country_id").val();
       }
    
     });

    });


function preview_images() 
{
 var total_file=document.getElementById("images").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
 }
}

 </script>
@component('components.form-submit')
      @slot('form')
          form
      @endslot

      @slot('redirect')
        {{ route('application.index') }}
      @endslot
  @endcomponent
@endsection