@extends('auth.master')
@section('content')
<div class="block-center mt-xl wd-xl login" id="card">
 <!-- START panel-->
 <div class="panel panel-flat front signin_form">
  <div class="panel-heading text-center">
   <div class="login-icon">
     <img src="{{ asset('img/User-Circle.png') }}" alt="" width="100">
   </div>
   <h2>Reset Password</h2>
 </div>
 <div class="panel-body">
  <form action="{{ route('password.request') }}" method="post" class="mb-lg" >
   @csrf
    <input type="hidden" name="token" value="{{ Token::generate(); }}">
   @component('components.warning-alert')
   @endcomponent
    <div class="form-group has-feedback">
    <span class="icon-user form-control-feedback text-muted"></span>
    <input name="email" type="email" placeholder="Email" class="form-control">
    
  </div>
   <div class="form-group has-feedback">
   <span class="icon-lock form-control-feedback text-muted"></span>
   <input name="password" type="password" placeholder="Password" class="form-control">
   
 </div>
  
 <div class="form-group has-feedback">
   <span class="icon-lock form-control-feedback text-muted"></span>
   <input name="password_confirmation" type="confirm_password" placeholder="Password" class="form-control">
   
 </div>
 
<div class="clearfix">
 
 <div class="pull-right" style="color:#FFFFFF"><a href="{{ route('register') }}" class="text-muted">{{ __('Register') }}</a>
 <div class="pull-left" style="color:#FFFFFF"><a href="{{ route('login') }}" class="text-muted">{{ __('login') }}</a>
 </div>
</div>
<button type="submit" class="btn btn-block btn-ig mt-lg">{{ __('Reset Password') }}</button>
</form>
</div>
</div>
<!-- END panel-->
</div>
@endsection

@section('custom_js')

@endsection