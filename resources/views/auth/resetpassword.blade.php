@extends('auth.master')
@section('content')
<div class="block-center mt-xl wd-xl login" id="card">
 <!-- START panel-->
 <div class="panel panel-flat front signin_form">
  <div class="panel-heading text-center">
   <div class="login-icon">
     <img src="{{ asset('img/User-Circle.png') }}" alt="" width="100">
   </div>
   <h2>Reset Password</h2>
 </div>
 <div class="panel-body">
  <form action="{{ route('register.reset') }}" method="post" class="mb-lg" id="form">
   @csrf
    @method('PUT')
    @if($errors->all())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>{{ $errors->all()[0] }}</strong>
                </div>
                @endif
   
    <div class="form-group has-feedback">
    <span class="icon-user form-control-feedback text-muted"></span>
    <input name="email" type="email" placeholder="Email" class="form-control">
     
  </div>
   <div class="form-group has-feedback">
   <span class="icon-lock form-control-feedback text-muted"></span>
   <input name="password" type="password" placeholder="Password" class="form-control">
      
   
 </div>
  
 <div class="form-group has-feedback">
   <span class="icon-lock form-control-feedback text-muted"></span>
   <input name="password_confirmation" type="password" placeholder="Password" class="form-control">
   
 </div>
 
<div class="clearfix">
 
 <div class="pull-right" style="color:#FFFFFF"><a href="{{ route('register') }}" class="text-muted">{{ __('Register') }}</a> 
 </div>
 <div class="pull-left" style="color:#FFFFFF"><a href="{{ route('login') }}" class="text-muted">{{ __('login') }}</a>
 </div>
</div>
<button type="submit" class="btn btn-block btn-ig mt-lg">Reset Password</button>
</form>
</div>
</div>
<!-- END panel-->
</div>
@endsection

@section('custom_js')
@component('components.form-submit')
      @slot('form')
          form
      @endslot

      @slot('redirect')
        {{ route('application.index') }}
      @endslot
  @endcomponent
@endsection