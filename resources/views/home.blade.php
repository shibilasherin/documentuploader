
 @extends('layouts.master')

@section('content')
<div class="content-wrapper">
  <div class="content-heading">
     <span><em class="fa fa-file-text-o"></em>  </span> 
    
  </div>

  <div class="panel panel-default">
     <div class="panel-body">
      <ul class="nav nav-tabs">
     
        <li  class="active" ><a data-toggle="tab" href="#tabform">Dashboard </a></li>
     
      </ul>

      <div class="tab-content">
      
        <div id="tabform" class="tab-pane fade  in active ">

           <div class="container d-flex justify-content-center padding">
            @if(Auth::user()->level=='1')
    <div class="row">
        <div class="col-sm-4">
            <div class="progress blue"> <span class="progress-left"> <span class="progress-bar"></span> </span> <span class="progress-right"> <span class="progress-bar"></span> </span>
                <div class="progress-value">{{count($approved)}}</div>
              </div>
                <span class="align-text-center" style="text-align:center; position: absolute; right: 2%;
    left: 2%">Approved</span>
        
            </div>
       
         <div class="col-sm-4">
            <div class="progress red" > <span class="progress-left"> <span class="progress-bar"></span> </span> <span class="progress-right" > <span class="progress-bar"></span> </span>
                <div class="progress-value" style="border-color: #36ff04";>{{count($rejected)}}</div>
            </div>
            <span class="align-text-center" style="text-align:center; position: absolute; right: 2%;
    left: 2%">Rejected</span>
        
        </div>
         <div class="col-sm-4">
            <div class="progress green"> <span class="progress-left"> <span class="progress-bar"></span> </span> <span class="progress-right"> <span class="progress-bar"></span> </span>
                <div class="progress-value">{{count($pending)}}</div>
            </div>
            
        <span class="align-text-center" style="text-align:center; position: absolute; right: 2%;
    left: 2%">Pending</span>
        </div>
        @else
         <span class="align-text-center" style="text-align:center; position: absolute; right: 2%;
    left: 2%; font-color:red;font-size: x-large;  ">Welcome please fill your Application</span>
        
        @endif
    </div>
</div>

        </div>
      
      </div>
     </div>
  </div>
</div>
@endsection

@section('custom_css')
  <style>
    .form-group {
      padding: 0 15px;
    }


  </style>
@endsection

@section('custom_js')
  <script src="{{ asset('js/input-values.jquery.js') }}"></script>
  <script src="{{ asset('js/bootstrap-filestyle.js') }}"></script>
 

@endsection