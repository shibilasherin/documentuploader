@extends('layouts.master')
@section('content')
<div class="content-wrapper">

  <div class="content-heading">
   <span><i class="fa fa-user"></i> Profile Settings</span>
 </div>

 <div class="panel panel-default">
   <div class="panel-body">
    <form class="form-horizontal" action="{{ route('application.updateprofile') }}" method="post" id="form" enctype="multipart/form-data">
    @csrf
    @method('PUT')
     <div class="form-group">
       <div class="col-sm-4">
         <label class="control-label">
          First name
        </label>
        <input type="text" name="first_name" class="form-control" placeholder="First name" value="{{Auth::user()->first_name}}">
      </div>
      <div class="col-sm-4">
       <label class="control-label">
         Last name
       </label>
       <input type="text" name="last_name" class="form-control" placeholder="Last name" value="{{Auth::user()->last_name}}">
     </div>
     <div class="col-sm-4">
     <label class="control-label">
      Email
    </label>
    <input type="text" name="email" class="form-control" placeholder="Email" value="{{Auth::user()->email}}">
  </div>
   </div>
     
  
  
   
    <div class="form-group">
    
  <div class="col-sm-3">
     <label class="control-label">
      Password
    </label>
    <input type="password" name="password" class="form-control" placeholder="Password" value="">
  </div>
 <div class="col-sm-3">
     <label class="control-label">
     confirm Password
    </label>
    <input type="password" name="password_confirmation" class="form-control" placeholder="Password" value="">
  </div>

  <div class="col-sm-3">
   <label class="control-label"> Upload profile picture </label>
   {{-- <input type="file" name="documents[]" class="form-control filestyle"  onChange="readURL(this);"> --}}
   <input type="file" name="image" class="form-control filestyle"  onChange="readURL(this);" >
 <div>
 </div class="col-sm-3">
   <div class="imgbox-photo">
    <span class="helper"></span>
  <img src="@if(file_exists(Auth::user()->image)) {{ asset(Auth::user()->image) }} @else {{ asset(Auth::user()->image) }} @endif" alt="" class="previmg" id="blah">
  
  </div>
 </div>
</div>
  
<div class="form-group">
 <div class="col-sm-12">
  <div class="pull-right">
   <button class="btn btn-info btn-lg" type="submit">Submit</button>
 </div>
</div>
</div>
</form>

</div>
</div>
</div>
@endsection

@section('custom_css')
  <style>
    .form-group {
      padding: 0 15px;
    }


  </style>
@endsection

@section('custom_js')
  <script src="{{ asset('js/input-values.jquery.js') }}"></script>
  <script src="{{ asset('js/bootstrap-filestyle.js') }}"></script>
 
 <script type="text/javascript">
   $('#city_id').change(function() {
     var city =$(this).val();
     // alert(city);
        var formData = new FormData();
        formData.append('city',city);
        formData.append("_token", '{{ csrf_token() }}');
     $.ajax({
       url:'{{ route('application.getstate') }}',
       type: 'POST',
       dataType: 'json',
       data:formData,
       processData: false,
       contentType: false
     })
     
     .done(function(data) {
       if(data.status=="success")
       {
      alert(data.state.id);

         $("#state_id").val(data.state.name);
         $("#country_id").val(data.country.name);
             
       }
       else
       {
         $("#state_id").val();
         $("#country_id").val();
       }
    
     });

    });




 </script>
@component('components.form-submit')
      @slot('form')
          form
      @endslot

      @slot('redirect')
        {{ route('application.index') }}
      @endslot
  @endcomponent
@endsection