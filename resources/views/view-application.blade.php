


 @extends('layouts.master')

@section('content')
<div class="content-wrapper">

  <div class="content-heading">
   <span><i class="fa fa-file"></i> View application</span>
 </div>

 <div class="panel panel-default">
   <div class="panel-body">

@if(Auth::user()->level=='3')
    <div class="row">
     <div class="col-lg-12" style="height: 100%;">
        <span class="align-text-center" style="text-align:center; position: absolute; right: 2%;
    left: 2%; font-size: x-large; top:5%; bottom:5%; ">Thank your for your Application</span>
     </div>
    <br>
    </div>
@else
    <div class="table-responsive">
      <table class="datatable table table-striped table-hover">
       <thead>
        <tr>
         <th>#</th>
         <th>First name</th>
         <th>Last name</th>
         <th>Phone</th>
         <th>Email</th>
         <th>city</th>
        
         <th style="min-width: 100px;">Action</th>
       </tr>
     </thead>
     <tbody>
      @foreach($applicationlist as $row)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $row->first_name }}</td>
        <td>{{ $row->last_name }}</td>
        <td>{{ $row->phone }}</td>
        <td>{{ $row->email }}</td>
        <td>{{ $row->city->name }}</td>
        

        <td>
          <a href="{{ route('application.edit',['id'=>$row->id]) }}" class="btn btn-info btn-xs">Edit</a>
         
          {{-- <button class="btn btn-xs {{ ($row->status==1) ? 'btn-success' : 'btn-danger' }}" rel1="{{ $row->status }}" rel2="{{ $row->id }}" onclick="changeStatus(this);">{{ ($row->status==1) ? 'Active' : 'Inactive' }}</button> --}}

          <button class="btn btn-xs @if($row->status==1) btn-success @elseif($row->status==2) btn-info @else btn-danger @endif" rel1="{{ $row->status }}" rel2="{{ $row->id }}" onclick="changeStatus(this);">@if($row->status==1) pending @elseif($row->status==2) Approved @else Rejected @endif </button>
          <form action="{{ route('application.status') }}" method="post" id="form-status{{ $row->id }}" style="display: none;">
          @csrf
          @method('put')
          <input type="hidden" name="id" value="{{ $row->id }}">
          <input type="hidden" name="email" value="{{ $row->email }}">
          <input type="hidden" name="status" value="{{ $row->status }}">
          <button class="btn btn-danger" type="submit">Status</button>
          </form>
          
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>
@endif


</div>
</div>
</div>
@endsection

@section('custom_css')
  <style>
    .form-group {
      padding: 0 15px;
    }


  </style>
@endsection

@section('custom_js')
  <script src="{{ asset('js/input-values.jquery.js') }}"></script>
  <script src="{{ asset('js/bootstrap-filestyle.js') }}"></script>
 
 <script>
  function changeStatus(ele) {
    var status = $(ele).attr('rel1');
    var id = $(ele).attr('rel2');
    
    sweetAlert({
      title: "Are you sure?",
      /*text: "You will not be able to recover this banner!",*/
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: true,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {
        $.ajax({
          url: '{{ route('application.status') }}',
          type: 'POST',
          data: {id:id,status:status,_token:'{{ csrf_token() }}'}, 
          dataType: 'json',      
          success: function(msg)
          {
            $(ele).attr('rel1',msg.status);
            if(msg.status == 1){
              $(ele).addClass('btn-success');
              $(ele).removeClass('btn-warning');
              $(ele).removeClass('btn-info');
              $(ele).text('Pending');
            }
            elseif(msg.status==2)
            {
              $(ele).removeClass('btn-success');
              $(ele).addClass('btn-warning');
              $(ele).removeClass('btn-info');
              $(ele).text('Approved');
            }
            {
              $(ele).addClass('btn-info');
              $(ele).removeClass('btn-warning');
              $(ele).removeClass('btn-success');
              $(ele).text('Rejected');
            }
          }
        });
      } else {
          sweetAlert('Cancelled!', "", "success");
      }
    });
  }

</script>
 

@endsection