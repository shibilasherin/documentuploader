<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    //
    protected $fillable = [
        'first_name','last_name', 'email','father_name','phone','email','address','city_id','user_id','status','documents'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }
}
