<?php

namespace App\Http\Controllers;

use App\City;
use App\State;
use App\Country;
use App\User;
use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;


class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $applicationlist = Application::latest()->get();
        return view('view-application',compact('applicationlist'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $data['city'] = City::get();
        return view('application',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $rules = [
            'first_name' => 'required|string|max:191',
            'father_name' => 'required|string|max:191',
            'city_id' => 'required',
            'email' => 'required|email|unique:applications',
            'address' => 'required|string',
            
        ];

        $validation = Validator::make($request->all(), $rules);

        if($validation->fails())
        {
            $errors = $validation->errors();
            $ajax['status'] = "error";
            $ajax['msg'] = $errors->all()[0];
        }
        else
        {
            $path = NULL;
            $files[] ="";
            if($request->hasFile('documents'))
            {
                // $file = $request->file('documents');
                // $path = $request->documents->store('storage/uploads','public');
            
             foreach ($request->documents as $key => $file) {
                $filename = $file->getClientOriginalName();

                // $path = $file->storeAs('public/uploads/',$filename);
                // $files[] = str_replace('public/', 'storage/', $path);
                  $path = $file->move(public_path().'/uploads/', $filename);  
                $files[]= 'uploads/'.$filename ;  

                 

               }
            }
            $data= new Application;
            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->father_name = $request->father_name;
            $data->phone = $request->phone;
            $data->email = $request->email;
            $data->address = $request->address;
            $data->city_id = $request->city_id;
            $data->user_id = auth()->id();
            $data->documents = json_encode($files);
            $data->save();

            // $ajax['id'] = $data->id;
            $ajax['status'] = "success";
            $ajax['msg'] = "Application Submitted successfully";
        }

        echo json_encode($ajax);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::get();
        $application=Application::find($id);
        return view('application-edit',compact('city','application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'first_name' => 'required|string|max:191',
            'father_name' => 'required|string|max:191',
            'city_id' => 'required',
            'email' => 'required|email|unique:applications,email,'.$id,
            'address' => 'required|string',
            
        ];

        $validation = Validator::make($request->all(), $rules);

        if($validation->fails())
        {
            $errors = $validation->errors();
            $ajax['status'] = "error";
            $ajax['msg'] = $errors->all()[0];
        }
        else
        {  
           $files[] ="";
            $path = NULL;
            if($request->hasFile('documents'))
            {
                
             foreach ($request->documents as $key => $file) {
                $filename = $file->getClientOriginalName();

                // $path = $file->storeAs('public/uploads/',$filename);
                // $files[] = str_replace('public/', 'storage/', $path);
               $path = $file->move(public_path().'/uploads/', $filename);  
                $files[]=  'uploads/'.$filename ;  


                 

               }
               
            }
            $data=  Application::find($id);
            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->father_name = $request->father_name;
            $data->phone = $request->phone;
            $data->email = $request->email;
            $data->address = $request->address;
            $data->city_id = $request->city_id;
            $data->user_id = auth()->id();
            $data->documents = json_encode($files);
            $data->save();

            // $ajax['id'] = $data->id;
            $ajax['status'] = "success";
            $ajax['msg'] = "Application Submitted successfully";
        }

        echo json_encode($ajax);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getstate(Request $request)
    {
        $city= City::find($request->city);
        $state= State::find($city->state_id);
        $country = Country::find($state->country_id);
        $ajax['state']=$state;
        $ajax['country']=$country;
        $ajax['status'] = "success";
        echo json_encode($ajax);
    }
    public function status(Request $request)
    {
       
       if($request->status='1')
       {
        $status ='2';
       } 
      if($request->status='2')
       {
        $status = '3';
       }
      if($request->status='3')
       {
         $status ='1';
       }

       
       $application = Application::find($request->id);
       $application->status = $status;
       $application->save();
       return redirect()->back()->with('status', 'Status has been updated');
    }
     public function profilesetting()
    {

        return view('settings');
    }
    public function updateprofile(Request $request)
    {
       
        $rules = [
            'email' => 'required|email|unique:users,email,'.auth()->id(),
            'password' => 'required|string|min:6|confirmed',
            
        ];
      
        $validation = Validator::make($request->all(), $rules);

        if($validation->fails())
        {
            $errors = $validation->errors();
            $ajax['status'] = "error";
            $ajax['msg'] = $errors->all()[0];
        }
        else
        {
            $path = NULL;
            if($request->hasFile('image'))
            {
                $file = $request->file('image');
                // $path = $request->image->store('storage/uploads','public');
                // $path = Storage::putFile('uploads', $request->file('image'));
                 $filename = $file->getClientOriginalName();

               $path = $file->move(public_path().'/uploads/', $filename);  
                $files=  'uploads/'.$filename ;  
            }


            

        $user = User::find(auth()->id());
        $user->password = Hash::make($request->password);
        $user->image=$files;
        $user->save();
        $ajax['status'] = "success";

    }
    echo json_encode($ajax);
      
    
    }
   
}
