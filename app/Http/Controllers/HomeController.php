<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pending'] = Application::where('status',1)->get();
        $data['approved'] = Application::where('status',2)->get();
        $data['rejected'] = Application::where('status',3)->get();
        return view('home',$data);
    }
}
